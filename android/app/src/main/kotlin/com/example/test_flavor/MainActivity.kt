package com.example.test_flavor

import android.os.Bundle
import com.google.firebase.FirebaseApp
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel


class MainActivity: FlutterActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FirebaseApp.initializeApp(this)
    }

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(
            flutterEngine.dartExecutor,
            "flavor"
        ).setMethodCallHandler { call, result ->
            if(call.method == "getFlavor")
            {
                result.success("${BuildConfig.FLAVOR}-${BuildConfig.BUILD_TYPE}")
            }

        }
    }

}
