import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  String? flavor = await getFlavor();
  await Firebase.initializeApp();
  runApp(MyApp(flavor));
}

Future<String?> getFlavor() {
  // call sang native để lấy thông tin môi trường
  return const MethodChannel('flavor').invokeMethod<String>('getFlavor');
}

class MyApp extends StatefulWidget {
  String? flavor;

  MyApp(this.flavor, {Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState()  {
    // TODO: implement initState
    super.initState();
    FirebaseAnalytics analytics = FirebaseAnalytics.instance;
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      analytics.logAppOpen().then((value) => {

      });
    });

  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Flavor = ${widget.flavor}"),
            ],
          ),
        ),
      ),
    );
  }
}
